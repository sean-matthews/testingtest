import pytest
from unittest.mock import Mock
import os
import sys
from inspect import getsourcefile

current_path = os.path.abspath(getsourcefile(lambda: 0))
current_dir = os.path.dirname(current_path)
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
sys.path.append(parent_dir)

import src.sum67 as sum67


def test_sum67():
    assert sum67.sum67([1, 2, 2]) == 5
    assert sum67.sum67([6, 8, 1, 6, 7]) == 0
    assert sum67.sum67([6, 7, 1, 6, 7, 7]) == 8



