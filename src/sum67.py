def sum67(nums):
    """
    Return the sum of the numbers in the array,
    except ignore sections of numbers starting with a 6 and extending to the next 7
    (every 6 will be followed by at least one 7).
    Return 0 for no numbers.
    :param nums:
    :return int:
    """
    sum = 0
    counting = True
    for i in nums:
        if counting:
            if i == 6:
                counting = False
            else:
                sum += i
        else:
            if i == 7:
                counting = True
    return sum
